# TYZYN

Este proyecto ha sido creado como backend de la aplicación  [tyzyn.com](https://www.tyzyn.com).

Tyzyn es un entorno para que los músicos puedan compartir sus creaciones y buscar nuevos miembros. También permite la búsqueda de eventos.

## ARQUITECTURA.

La arquitectura la componen un servidor UBUNTU con apache y mysql. Se utilizará symfony y api platform para el desarrollo.

## API

La API tiene varias funcionalidades:


* [ ] Registro.
* [ ] Inicio de sesión.

